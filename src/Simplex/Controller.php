<?php
namespace Simplex;

use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;

class Controller
{
    /**
     * Render template.
     *
     * @param $filename
     * @param array $parameters
     * @return string
     */
    public function render($filename, $parameters = [])
    {
        $class = new \ReflectionClass(get_called_class());
        $path = $class->getFileName();
        $viewPath = $path;
        $pathArr = explode("\\", $path);
        $pathArr = array_slice($pathArr, 0, count($pathArr) - 2);
        $viewPath = implode('\\', $pathArr);

        $loader = new \Twig_Loader_Filesystem($viewPath . '\views');
        $twig = new \Twig_Environment($loader, array(
            'cache' => 'C:\Development\symf-framework\cache\twig\compilation_cache',
        ));

        $template = $twig->load($filename);
        $response = $template->render($parameters);
        return $response;
    }
}
