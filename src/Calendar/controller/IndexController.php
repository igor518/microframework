<?php
namespace Calendar\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Simplex\Controller;

class IndexController extends Controller
{
    /**
     * @return mixed
     */
    public function indexAction()
    {
        $response = [];
        $response['version'] = 'API v 1.0';
        $response['type'] = 'TEST';
        $response['content'] = 'None';

        return new JsonResponse($response);
    }
}
